//
// Created by Rulin Tang on 15/12/18.
//
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


int main() {
    int sockfd, new_fd;
    struct sockaddr_in server_addr;
    struct sockaddr_in client_addr;
    socklen_t sin_size;
    int port = 4444;

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        fprintf(stderr, "Socket error:%s\n\a", strerror(errno));
        exit(1);
    }

    int opt;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

    bzero(&server_addr, sizeof(struct sockaddr_in));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(port);

    if (bind(sockfd, (struct sockaddr *) (&server_addr), sizeof(struct sockaddr)) == -1) {
        fprintf(stderr, "Bind error:%s\n\a", strerror(errno));
        exit(1);
    }

    if (listen(sockfd, 5) == -1) {
        fprintf(stderr, "Listen error:%s\n\a", strerror(errno));
        exit(1);
    }
    while (1) {
        char data[1024];
        /* 服务器阻塞,直到客户程序建立连接  */
        sin_size = sizeof(struct sockaddr_in);
        if ((new_fd = accept(sockfd, (struct sockaddr *) (&client_addr), &sin_size)) == -1) {
            fprintf(stderr, "Accept error:%s\n\a", strerror(errno));
            exit(1);
        }

        fprintf(stderr, "Server get connection from %s\n",
                inet_ntoa(client_addr.sin_addr));
        if (read(new_fd, data, 1024) == -1) {
            fprintf(stderr, "Read Error:%s\n", strerror(errno));
            exit(1);
        }

        printf("%d\n", (int) strlen(data));
        printf("%s", data);

        if (write(new_fd, data, strlen(data)) == -1) {
            fprintf(stderr, "Write Error:%s\n", strerror(errno));
            exit(1);
        }
        /* 这个通讯已经结束     */
        close(new_fd);
        /* 循环下一个     */
    }
    close(sockfd);
    exit(0);
}